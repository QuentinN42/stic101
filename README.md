# STIC101 - Tables Rondes métiers

# Presentation

Dans le cadre de la formation, un cycle de conférence-débat permettant la découverte des métiers de l'ingénieur est proposé aux élèves et aux doctorants.
Différentes thématiques liées aux métiers, aux entreprises, et à des questions scientifiques contemporaines sont abordées par des professionnels, des enseignants-chercheurs, les échanges étant animés par un élève de 2e année.

Les élèves de 1e année doivent assister dans l'année à minimum 6 sur 10 de ces conférences.
Ils devront rédiger en fin d'année une synthèse de ce que leur ont apporté ces conférences-débat dans la construction de leur projet scolaire et/ou professionnel.
Il ne s'agira pas de restituer le contenu des conférences mais d'en tirer, comme pour les visites Entreprises, un bilan sur ce qu'elle ont apporté.

# Consignes :

Sans reprendre le contenu des conférences, faites le bilan de ce que vous avez retenu, ce que vous en retenez pour vous-même.

Pistes de réflexion :
- Qu'est-ce qui m'a plu ? déplu ?
- Qu'est-ce qui m'a surpris ?
- Qu'est-ce que cela m'a apporté pour mon orientation et pour mon projet professionnel ?

Repensez à votre écrit suite aux visites virtuelles d'entreprises de novembre dernier et mesurez l'évolution de votre réflexion.
C'est un exercice personnel qui vous permettra de faire le point en cette fin de 1e année sur la construction de votre projet professionnel.
