# Positionnement pré-conférences
Étant donné que je ne viens pas de CPGE mais de licence,
j'ai eut plus de temps pour réfléchir a mon projet professionnel.
Je savait donc avant mon entrée à Télécom Paris que j'étais intéressé par le domaine informatique.
Plus précisément, le lien entre développeurs et sysadmins m'intéresse particulièrement :

- Le lien entre ces deux domaines demande de nombreuses compétences transversales.
- Les technologies changent rapidement.
- Avec l'explosion du cloud, ce milieu est en perpétuelle évolution.

De plus il y a une certaine stimulation à donner vie a un code.
Faisant passer un simple fichier texte inerte à une application que les utilisateurs peuvent réellement utiliser.


# Conférences
Parmi les conférences que j'ai suivies, j'ai particulièrement apprécié les trois suivantes :
- Blockchain
- Cybersécurité
- Entreprenariat

Ces conférences m'on conforté dans mes perspectives précédentes :
il a été plusieurs fois souligné durant ces conférences que la mise en production
d'un logiciel n'est jamais une chose aisée mais toujours stimulate avec de nombreuses
acquisitions de compétences tout au long de sa carrière.


## Blockchain
J'ai pu découvrir des applications de la blockchain que je ne soupçonnait pas :
On peut faire tourner de petits bout de code sur Ethereum,
pour moi cette nouvelle a totalement expliqué l'évolution exponentielle des blockchains.
En effet, une blockchain n'est donc plus un simple espace de stockage incroyablement sécurisé,
mais aussi un super calculateur distribué !
Il ne serait donc pas étonnant de voir ce genre de programmes se démocratiser dans les futures années.
Cette conférence a donc fait significativement remonter dans ma TODO list l'apprentissage et l'implémentation d'une blockchain. 


## Cybersécurité
Assister a la conférence cybersécurité avec le CSR de Orange Cyberdefense a aussi été une formidable occasion
de parler du sujet sensible qu'est la sécurité des systèmes informatiques :
Du côté business, la priorité est à l'évolution / l'ajout de nouvelles fonctionnalités, cependant chaque ligne de code est
une faille potentielle pouvant mener a la ruine de l'enterprise...
Mais aucuns système est infaillible, il est donc très long d'auditer un logiciel pour avoir une très grande fiabilité sur sa sureté.
Il faut donc réussir à trouver un juste milieu entre sécurité et fonctionnalités.

Cette problématique est mise en exergue par le development dans le cloud qui prend une part de plus en plus importante.
La mutualisation des serveurs est positif dans de nombreux sens mais laisse la place a des failles potentielles.
Il faut donc bien connaitre l'architecture d'un serveur cloud pour pouvoir détecter en amont celles-ci.

Pour développer mes compétences dans ce domaine, j'ai choisi de prendre la filière SR2I de 2A.


## Entreprenariat
Durant la conférence sur l'entreprenariat, j'ai eut la chance de pouvoir échanger avec le créateur du site du zero et de open classroom.
J'ai découvert l'informatique grace a ces sites et je me forme encore aujourd'hui grace a ces MOOCs très bien réalisés
(dommage que Télécom ne propose pas une UE MOOC Open Classroom ;).

Cette conférence m'a aussi beaucoup influence dans mon parcours académique car elle m'a donnée envie de faire de l'alternance.
J'ai donc cherché une petite entreprise pour me retrouver dans un environment extrêmement stimulant où il est obligatoire
de sortir de sa zone de confort car le faible nombre d'employés impose une versatilité extreme.

J'ai trouvé une petite startup où je serais alternant consultant cloud.
Ce milieu m'intéresse particulièrement car c'est ici que se déploie le plus grand nombre de services.


# Conclusion
Comme vus précédemment, les conférences m'ont beaucoup aidé à orienter mes choix pour la seconde année.
De manière générale, ces conférences sont un magnifique endroit d'échange entre étudiant et ingénieurs expérimenté,
nous permettant de situer nos apprentissages académiques dans un contexte entrepreneurial actuel.

La diversité des intervenants permet d'avoir une vue d'ensemble assez complete du monde de l'entreprise ce qui est
extrêmement positif pour s'orienter où s'ouvrir a de nouvelles opportunités.

Cependant nous avons eut des conférences moins intéressantes durant lesquels nous n'avons pas eut l'occasion de parler
du sujet de la conférence en profondeur car les intervenants n'étaient pas des ingénieurs mais des commerciaux venus
nous venter les bienfaits de leur entreprise. Pour résoudre ce problème il faudrait pouvoir choisir le poste de l'intervenant
afin de prendre des profils plus techniques et donc avoir des discutions plus intéressantes.
